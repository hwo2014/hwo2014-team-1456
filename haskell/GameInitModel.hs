{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE OverloadedStrings #-}
module GameInitModel where
------------------------------------------------------------------------------

import Data.Aeson ((.:), (.:?), decode, encode, (.=), object,
    FromJSON(..), ToJSON(..), Value(..))
import Control.Applicative ((<$>), (<*>))

join sep []     = []
join sep (x:xs) = x ++ join' xs
    where
        join' []     = []
        join' (x:xs) = sep ++ x ++ join' xs

------------------------------------------------------------------------------
data Dimension = Dimension {
    dimensionLength   :: Float,
    width             :: Float,
    guideFlagPosition :: Float
} deriving (Show)

instance FromJSON Dimension where
    parseJSON (Object v)
        = Dimension
            <$> (v .: "length")
            <*> (v .: "width")
            <*> (v .: "guideFlagPosition")

------------------------------------------------------------------------------
data CarId = CarId {
    color     :: String,
    carIdName :: String
} deriving (Show)

instance FromJSON CarId where
    parseJSON (Object v)
        = CarId
            <$> (v .: "color")
            <*> (v .: "name")


------------------------------------------------------------------------------
data Car = Car {
    carId      :: CarId,
    dimensions :: Dimension
} deriving (Show)

instance FromJSON Car where
    parseJSON (Object v)
        = Car
            <$> (v .: "id")
            <*> (v .: "dimensions")

------------------------------------------------------------------------------
data Lane = Lane {
    distanceFromCenter :: Int,
    index              :: Int
} deriving (Show)

instance FromJSON Lane where
    parseJSON (Object v)
      = Lane
          <$> (v .: "distanceFromCenter")
          <*> (v .: "index")

------------------------------------------------------------------------------
data Piece = Piece {
    pieceLength :: Maybe Float,
    switch      :: Maybe Bool,
    radius      :: Maybe Int,
    pieceAngle  :: Maybe Float,
    bridge      :: Maybe Bool
}

instance Show Piece where
    show (Piece length switch radius angle bridge)
        = join "-" [ x | Just x <- [maybeshow "L" length,
                                    maybeshow "S" switch,
                                    maybeshow "R" radius,
                                    maybeshow "A" angle,
                                    maybeshow "B" bridge
                        ]
                    ]
        where

            maybeshow tag Nothing = Nothing
            maybeshow tag (Just that) = Just (tag ++ show that)


instance FromJSON Piece where
    parseJSON (Object v)
        = Piece
            <$> (v .:? "length")
            <*> (v .:? "switch")
            <*> (v .:? "radius")
            <*> (v .:? "angle")
            <*> (v .:? "bridge")

------------------------------------------------------------------------------
data StartingPoint = StartingPoint {
    position :: Position,
    angle    :: Float
} deriving (Show)

instance FromJSON StartingPoint where
    parseJSON (Object v)
        = StartingPoint
            <$> (v .: "position")
            <*> (v .: "angle")

------------------------------------------------------------------------------
data Position = Position {
    x :: Float,
    y :: Float
} deriving (Show)

instance FromJSON Position where
    parseJSON (Object v)
        = Position
            <$> (v .: "x")
            <*> (v .: "y")

------------------------------------------------------------------------------
data Track = Track {
    name          :: String,
    startingPoint :: StartingPoint,
    pieces        :: [Piece],
    lanes         :: [Lane]
} deriving (Show)

notrack = Track "foo" (StartingPoint (Position 0 0) 0) [] []

instance FromJSON Track where
    parseJSON (Object v)
        = Track
            <$> (v .: "name")
            <*> (v .: "startingPoint")
            <*> (v .: "pieces")
            <*> (v .: "lanes")

------------------------------------------------------------------------------
data RaceSession = RaceSession {
  laps          :: Maybe Int,
  durationMs    :: Maybe Int,
  maxLapTimeMs  :: Maybe Int,
  quickRace     :: Maybe Bool
} deriving (Show)

instance FromJSON RaceSession where
    parseJSON (Object v)
        = RaceSession
            <$> (v .:? "laps")
            <*> (v .:? "durationMs")
            <*> (v .:? "maxLapTimeMs")
            <*> (v .:? "quickRace")

------------------------------------------------------------------------------
data Race = Race {
    track :: Track,
    cars  :: [Car],
    raceSession :: RaceSession
} deriving (Show)

instance FromJSON Race where
    parseJSON (Object v)
        = Race
            <$> (v .: "track")
            <*> (v .: "cars")
            <*> (v .: "raceSession")

------------------------------------------------------------------------------
data GameInitData = GameInitData {
    race :: Race
} deriving (Show)

instance FromJSON GameInitData where
    parseJSON (Object v)
        = GameInitData <$> (v .: "race")

------------------------------------------------------------------------------
-- Helpers
------------------------------------------------------------------------------
players :: GameInitData -> [CarId]
players gameInit = map (\car -> carId $ car) $ cars $ race gameInit

piecesOfGame :: GameInitData -> [Piece]
piecesOfGame gameInit = pieces $ track $ race gameInit

lanesOfGame :: GameInitData -> [Lane]
lanesOfGame gameInit = lanes $ track $ race gameInit

reportGameInit :: GameInitData -> String
reportGameInit gameInit
    =  "Players: " ++ (show $ players gameInit)
    ++ ", Track: " ++ show (length $ piecesOfGame gameInit) ++ " pieces"
