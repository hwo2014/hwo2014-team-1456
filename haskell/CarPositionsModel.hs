{-# LANGUAGE OverloadedStrings #-}
module CarPositionsModel where
------------------------------------------------------------------------------

import GameInitModel(CarId(..))

import Data.Aeson ((.:), (.:?), decode, encode, (.=), object, FromJSON(..), ToJSON(..), Value(..))
import Control.Applicative ((<$>), (<*>))
import Control.Monad        (liftM)
import Data.List

------------------------------------------------------------------------------
data CarLane = CarLane {
    startLaneIndex :: Int,
    endLaneIndex   :: Int
} deriving (Show)

instance FromJSON CarLane where
    parseJSON (Object v)
        = CarLane
            <$> (v .: "startLaneIndex")
            <*> (v .: "endLaneIndex")

------------------------------------------------------------------------------
data PiecePosition = PiecePosition {
    pieceIndex      :: Int,
    inPieceDistance :: Float,
    lane            :: CarLane,
    lap             :: Int
} deriving (Show)

instance FromJSON PiecePosition where
    parseJSON (Object v)
        = PiecePosition
            <$> (v .: "pieceIndex")
            <*> (v .: "inPieceDistance")
            <*> (v .: "lane")
            <*> (v .: "lap")

------------------------------------------------------------------------------
data CarPosition = CarPosition {
    carId         :: CarId,
    angle         :: Float,
    piecePosition :: PiecePosition
} deriving (Show)

instance FromJSON CarPosition where
    parseJSON (Object v)
        = CarPosition
            <$> (v .: "id")
            <*> (v .: "angle")
            <*> (v .: "piecePosition")

------------------------------------------------------------------------------
data Turbo = Turbo {
    turboDurationTicks        :: Int,
    turboDurationMilliseconds :: Int,
    turboFactor               :: Int
} deriving (Show)

instance FromJSON Turbo where
    parseJSON (Object v)
        = Turbo
            <$> (v .: "turboDurationTicks")
            <*> (v .: "turboDurationMilliseconds")
            <*> (v .: "turboFactor")

------------------------------------------------------------------------------
data RaceTime = RaceTime {
    raceTimeTicks       :: Int,
    raceTimeMillis      :: Int,
    raceTimeLaps        :: Int
} deriving (Show)

instance FromJSON RaceTime where
    parseJSON (Object v)
        = RaceTime
            <$> (v .: "ticks")
            <*> (v .: "millis")
            <*> (v .: "laps")

------------------------------------------------------------------------------
data LapTime = LapTime {
    lapTimeTicks       :: Int,
    lapTimeMillis      :: Int,
    lapTimeLap         :: Int
} deriving (Show)

instance FromJSON LapTime where
    parseJSON (Object v)
        = LapTime
            <$> (v .: "ticks")
            <*> (v .: "millis")
            <*> (v .: "lap")

------------------------------------------------------------------------------
data Ranking = Ranking {
    fastestLap :: Int,
    overall    :: Int
} deriving (Show)

instance FromJSON Ranking where
    parseJSON (Object v)
        = Ranking
            <$> (v .: "fastestLap")
            <*> (v .: "overall")

------------------------------------------------------------------------------
data LapFinish = LapFinish {
    lapFinishRaceTime :: RaceTime,
    lapFinishLapTime  :: LapTime,
    lapFinishCar      :: CarId,
    lapFinishRanking  :: Ranking
} deriving (Show)

instance FromJSON LapFinish where
    parseJSON (Object v)
        = LapFinish
            <$> (v .: "raceTime")
            <*> (v .: "lapTime")
            <*> (v .: "car")
            <*> (v .: "ranking")

------------------------------------------------------------------------------
-- Helpers
------------------------------------------------------------------------------
findCar :: String -> [CarPosition] -> Maybe CarPosition
findCar carName positions = find nameMatches positions
    where
        nameMatches carPosition = carIdName (carId carPosition) == carName
