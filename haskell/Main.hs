{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}
module Main where
------------------------------------------------------------------------------

import System.Environment (getArgs)
import System.Exit (exitFailure)

import Network(connectTo, PortID(..))
import System.IO(hPutStrLn, hGetLine, hSetBuffering, BufferMode(..), Handle)
import Data.List
import Control.Monad
import Control.Applicative
import qualified Data.ByteString.Lazy.Char8 as L
import Data.Aeson(decode, FromJSON(..), ToJSON(..), fromJSON, parseJSON,
        eitherDecode, Value(..), (.=),(.:), Result(..), object, encode)

import GameInitModel
import CarPositionsModel

------------------------------------------------------------------------------
--  Messages to send
------------------------------------------------------------------------------
data ClientMessage
    = JoinResponse { botname :: String, botkey :: String }
    | ThrottleResponse Double
    | PingResponse
    | TurboResponse
    deriving (Show)


jsonMessage :: ToJSON a => String -> a -> Value
jsonMessage msgType msgData = object [
    "msgType" .= msgType,
    "data"    .= msgData
 ]

instance ToJSON ClientMessage where
    toJSON message = case message of
        JoinResponse botname botkey
            -> jsonMessage "join" (object [
                "name" .= botname,
                "key" .= botkey
                ])
        ThrottleResponse amount
            -> jsonMessage "throttle" amount
        PingResponse
            -> jsonMessage "ping" (object [])
        TurboResponse
            -> jsonMessage "turbo" ("miu." :: String)


--joinResponse botname botkey
--    = "{\"msgType\":\"join\",\"data\":{\"name\":\""
--        ++ botname ++ "\",\"key\":\""
--        ++ botkey ++ "\"}}"

--throttleResponse amount
--    = "{\"msgType\":\"throttle\",\"data\":" ++ (show amount) ++ "}"

--pingResponse = "{\"msgType\":\"ping\",\"data\":{}}"

--turboResponse = "{\"msgType\":\"turbo\",\"data\":\"miu.\"}"

------------------------------------------------------------------------------
--  Main runner
------------------------------------------------------------------------------
main = do
    args <- getArgs
    case args of
        [server, port, botname, botkey] -> do
            run server port botname botkey
        _ -> do
            putStrLn "Usage: hwo2014bot <host> <port> <botname> <botkey>"
            exitFailure

data State = State {
    track :: Track
}


run server port botname botkey = do
    h <- connectToServer server port
    hSetBuffering h LineBuffering
    sendResponse h $ JoinResponse botname botkey
    handleMessages h (State notrack)

connectToServer server port
    = connectTo server (PortNumber (fromIntegral (read port :: Integer)))

sendResponse h response = do
    putStrLn $ "<--" ++ show response
    putStrLn $ "<--" ++ show (encode response)
    L.hPutStr h (encode response `L.append` "\n")

------------------------------------------------------------------------------
--  Message handling
------------------------------------------------------------------------------

handleMessages h state = do
    msg <- hGetLine h
    case decode (L.pack msg) of
        Just json ->
          let decoded = fromJSON json >>= decodeMessage in
          case decoded of
              Success serverMessage -> do
                state' <- handleServerMessage h serverMessage state
                handleMessages h state'
              Error s -> fail $ "Error decoding message: " ++ s
        Nothing -> do
          fail $ "Error parsing JSON: " ++ (show msg)

handleServerMessage :: Handle -> ServerMessage -> State -> IO State
handleServerMessage h serverMessage state = do
    if shouldShowMessage serverMessage then
        putStrLn $ "-->" ++ show serverMessage
        else return ()
    (state', responses) <- respond serverMessage state
    forM_ responses $ \response -> do
        sendResponse h response
    return state'

------------------------------------------------------------------------------
--  Messages
------------------------------------------------------------------------------

data ServerMessage
    = Join
    | YourCar CarId
    | Finish CarId
    | GameInit GameInitData
    | GameStart
    | CarPositions [CarPosition]
    | TurboAvailable Turbo
    | LapFinished LapFinish
    | Crash CarId
    | Unknown String Value
    deriving (Show)

shouldShowMessage message = case message of
   -- CarPositions _ -> False
    _              -> True

respond :: ServerMessage -> State -> IO (State, [ClientMessage])
respond message state = case message of
    CarPositions carPositions -> do
        return (state, [ThrottleResponse 0.63])
    GameInit (GameInitData (Race track cars session)) -> do
        return (State track, [PingResponse])
    --TurboAvailable turbo -> do
    --    return [TurboResponse]
    _ -> do
        return (state, [PingResponse])

------------------------------------------------------------------------------
--  Message decoding
------------------------------------------------------------------------------
decodeMessage :: (String, Value) -> Result ServerMessage
decodeMessage (msgType, msgData)
    | msgType == "join"           = Success Join
    | msgType == "gameStart"      = Success GameStart
    | msgType == "gameInit"       = GameInit       <$> (fromJSON msgData)
    | msgType == "carPositions"   = CarPositions   <$> (fromJSON msgData)
    | msgType == "yourCar"        = YourCar        <$> (fromJSON msgData)
    | msgType == "finish"         = Finish         <$> (fromJSON msgData)
    | msgType == "turboAvailable" = TurboAvailable <$> (fromJSON msgData)
    | msgType == "lapFinished"    = LapFinished    <$> (fromJSON msgData)
    | msgType == "crash"          = Crash          <$> (fromJSON msgData)
    | otherwise = Success $ Unknown msgType msgData

instance FromJSON a => FromJSON (String, a) where
    parseJSON (Object v) = do
        msgType <- v .: "msgType"
        msgData <- v .: "data"
        return (msgType, msgData)
    parseJSON x          = fail $ "Not an JSON object: " ++ (show x)
